import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);

//http://localhost:58718/
//http://10.3.6.16:8087

export default new Vuex.Store({
    state: {
        Empleado: null,
        EmpleadoPlaza: null,
        EmpleadoIdSeleccionado: null,
        PlazaIdSeleccionado: null,
        HistoriaPlazaCT: []
    },
    mutations: {
        SET_EMPLEADO(state, payload){
            state.Empleado = payload[0]; 
        },
        SET_EMPLEADO_ID_SELECCIONADO(state, payload){
            state.EmpleadoIdSeleccionado = payload; 
        },
        SET_EMPLEADO_PLAZA(state, payload){
            state.EmpleadoPlaza = payload; 
        },
        SET_PLAZA_ID_SELECCIONADO(state, payload){
            state.PlazaIdSeleccionado = payload; 
        },
        SET_HPCT(state, payload){
            state.HistoriaPlazaCT = payload;
        }
    },
    actions: {
        SET_EMPLEADO(context, payload){
            context.commit('SET_EMPLEADO', payload);
        },
        SET_EMPLEADO_ID_SELECCIONADO(context, payload){
            context.commit('SET_EMPLEADO_ID_SELECCIONADO', payload);
        },
        SET_EMPLEADO_PLAZA(context, payload){
            context.commit('SET_EMPLEADO_PLAZA', payload); 
        },
        SET_PLAZA_ID_SELECCIONADO(context, payload){
            context.commit('SET_PLAZA_ID_SELECCIONADO', payload);
        },
        SET_HPCT(context, payload){
            context.commit('SET_HPCT', payload);
        }

        

    }
})