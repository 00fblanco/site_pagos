
import VueRouter from 'vue-router';

import Consultas from './components/Consultas';

import CapturaSaycop from './components/CapturaSaycop';
import CalculoSaycop from './components/CalculoSaycop';


const routes = [
  { path: '/consultas', component: Consultas },
  { path: '/captura_saycop', component: CapturaSaycop },
  { path: '/calculo_saycop', component: CalculoSaycop },
]

const router = new VueRouter({
  routes // short for `routes: routes`
})



export default router;

/*
{ 
    path: '/dashboard', component: Dashboard ,
    beforeEnter: validToken 
  }
  ,
  { 
      path: '/JefeJuzgados', component: JefeJuzgados,
      beforeEnter: validToken,
      children: [
        {
          path: '/GestionUsuarios', component: GestionUsuarios
        }
      ]
    },
    { 
      path: '/', 
      beforeEnter: redirect
    },
    

    function redirect(to, from, next ){
  window.console.log('red')
  let usuario = window.Store.state.Usuario;
  if( usuario == undefined){
    next('login');
  }else{
    if(usuario.Rol.RolDescripcion === 'Jefe de los Juzgados'){
      next('JefeJuzgados');
    }
    
  }
}
function validToken(to, from, next){
  
  let usuario = window.Store.state.Usuario;
  if( usuario == undefined){
    next('login');
  }else{
    next();
  }
}
    */