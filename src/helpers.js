export default {

    desglozaCT(ct) {
        let ent_fed = parseInt(ct.substring(0, 2));
        let ct_clasif = ct.substring(2, 3);
        let ct_id = ct.substring(3, 5);
        var aux = ct.substring(5, ct.length);
        let digito_ver = Array.from(aux)[aux.length - 1];
        aux = aux.slice(0, -1);
        let ct_secuencial = parseInt(aux);

        return {
            cct: ct,
            ent_fed,
            ct_clasif,
            ct_id,
            digito_ver,
            ct_secuencial
        }
    },

    desglozaPlaza(plaza) {

        plaza = plaza.trim();

        const cod_pago = parseInt(plaza.substring(0, 2));
        const unidad = parseInt(plaza.substring(2, 4));
        const subunidad = parseInt(plaza.substring(4, 6));
        const horas = parseFloat(plaza.substring(6, 10));

        let categoria = "";
        let cons_plaza = "";
        switch (plaza.length) {
            case 23:
                //Console.WriteLine("22");
                categoria = plaza.substring(10, 17);
                cons_plaza = parseInt(plaza.substring(17,23));
                break;
            case 22:
                //Console.WriteLine("22");
                categoria = plaza.substring(10, 16);
                cons_plaza = parseInt(plaza.substring(16,22));
                break;
            case 21:
                //Console.WriteLine("21");
                categoria = plaza.substring(10, 15);
                cons_plaza = parseInt(plaza.substring(15,21));
                break;
        }

        return {
            cod_pago,
            unidad,
            subunidad,
            categoria,
            horas,
            cons_plaza
        }
    }
};