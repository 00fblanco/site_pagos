import App from './App.vue'
import Vue from 'vue'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import 'primevue/resources/themes/saga-blue/theme.css'       //theme
import 'primevue/resources/primevue.min.css' 
import 'primevue/resources/themes/saga-blue/theme.css'
import 'primeicons/primeicons.css'
import EvaIcons from 'vue-eva-icons'
import Vuex from 'vuex'
import PrimeVue from 'primevue/config';
import DataTable from 'primevue/datatable';
import Column from 'primevue/column';
import BlockUI from 'primevue/blockui';
import InputText from 'primevue/inputtext';
import InputNumber from 'primevue/inputnumber';

import Editor from 'primevue/editor';

import VueRouter from 'vue-router'
Vue.use(VueRouter)
Vue.use(ElementUI);
Vue.use(EvaIcons);
Vue.use(Vuex);
Vue.use(PrimeVue);
Vue.component('Editor', Editor);
Vue.component('DataTable', DataTable);
Vue.component('InputText', InputText);
Vue.component('InputNumber', InputNumber);
Vue.component('Column', Column);
Vue.component('BlockUI', BlockUI);
import './assets/theme/index.css'
import lang from 'element-ui/lib/locale/lang/en'
import locale from 'element-ui/lib/locale'

locale.use(lang)

import router from './router.js';
import store  from './store'


Vue.config.productionTip = false
new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')

