
import axios from 'axios';

//http://localhost:58718/
//http://10.3.6.16:8087
axios.defaults.baseURL = 'http://127.0.0.1:3333/';
axios.defaults.headers = {"Access-Control-Allow-Origin": "*"}

export default{

    fetchBuscaPorFC(rfc){
        return axios.get(`Empleado/PorRFC/${rfc}`)
            .then(res => {
                return res.data;
            });
    },
    fetchBuscaPlazas(EmpleadoId){
        return axios.get(`Empleado/Plazas/${EmpleadoId}`)
            .then(res => {
                return res.data;
            });
    },
    fetchBuscaEmpleadoPlaza(EmpleadoId,PlazaId){
        return axios.get(`Empleado/Plazas/${EmpleadoId}/${PlazaId}`)
            .then(res => {
                return res.data;
            });
    },
   
    fetchBuscaBeneficiariosPension(EmpleadoId){
        return axios.get(`Empleado/BeneficiariosPension/${EmpleadoId}`)
            .then(res => {
                return res.data;
            });
    },
    fetchPlazaMap(splaza){
        return axios.get(`Plaza/map/${splaza}`)
            .then(res => {
                return res.data;
            });
    },
    fetchBuscaHPCT(PlazaId){
        return axios.get(`Plaza/historia_plaza_ct/${PlazaId}`)
            .then(res => {
                return res.data;
            });
    },
    fetchBuscaPlazaEstatus(PlazaId){
        return axios.get(`Plaza/plaza_estatus/${PlazaId}`)
            .then(res => {
                return res.data;
            });
    },
    fetchBuscaPeriodosPago(plaza){
        return axios.get(`Pagos/${plaza}`)
            .then(res => {
                return res.data;
            });
    },
    fetchBuscaPagoPorFecha(plaza,fecha){
        return axios.get(`Pagos/${plaza}/${fecha}`)
            .then(res => {
                return res.data;
            });
    },
    fetchBuscarPagos(plaza){
        return axios.get(`Pagos/${plaza}`)
            .then(res => {
                return res.data;
            });
    },
    fetchBuscaConceptos(){
        return axios.get(`Concepto`)
            .then(res => {
                return res.data;
            });
    },
}